import pandas as pd
import numpy as np
data=pd.read_csv("BMJ Ch21.csv",usecols=['Item type','Cost ($)'], skipinitialspace = True)
data['Item type']=data['Item type'].str.upper()
print(data)

data2=pd.read_csv("bmj.csv",skipfooter=8,engine='python',skipinitialspace = True)
data2 = data2.rename(columns=data2.iloc[0]).loc[1:]
data2['Item type']=data2['Item type'].str.upper()
data2['Item type'] = data2['Item type'].str.replace(r"\(.*\)","")
data2['Item type'] = data2['Item type'].str.rstrip()
print(data2)

data2 = pd.merge(data2, data, how='inner', on = 'Item type')
print(data2)
data2.to_csv('newsheet.csv')